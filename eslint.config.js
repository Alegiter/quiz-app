import ts from "@typescript-eslint/eslint-plugin"
import tsParser from "@typescript-eslint/parser"
import vue from "eslint-plugin-vue"
import vueParser from "vue-eslint-parser"
import workspaces from "eslint-plugin-workspaces"
import stylisticTs from "@stylistic/eslint-plugin-ts"

export const ignores = {
    ignores: ["**/dist", "**/*.config.*"],
}

const workspacesConfig = {
    plugins: {
        workspaces,
    },
    rules: workspaces.configs.recommended.rules,
}

const stylisticTsConfig = {
    plugins: {
        "@stylistic/ts": stylisticTs
    },
    rules: {
        "@stylistic/ts/quotes": ["error", "double"],
        "@stylistic/ts/semi": ["error", "never"]
    }
}

export const tsConfig = {
    files: ["**/*.ts"],
    languageOptions: {
        parser: tsParser,
    },
    plugins: {
        "@typescript-eslint": ts,
    },
    rules: {
        ...ts.configs["eslint-recommended"].overrides[0].rules,
        ...ts.configs.recommended.rules,
    }
}


export const vueConfig = {
    files: ["**/*.vue"],
    languageOptions: {
        parser: vueParser,
        parserOptions: {
            parser: {
                ts: tsParser
            }
        }
    },
    plugins: {
        "@typescript-eslint": ts,
        "vue": vue,
    },
    rules: {
        ...tsConfig.rules,
        ...vue.configs["vue3-essential"].rules,
        ...vue.configs["vue3-recommended"].rules,
        ...vue.configs["vue3-strongly-recommended"].rules,
        "vue/max-attributes-per-line": ["error"],
        "vue/html-closing-bracket-spacing": ["error"],
        "vue/comment-directive": "off",
        "vue/multi-word-component-names": ["warn"]
    }
}


export default [
    ignores,
    workspacesConfig,
    stylisticTsConfig,
    tsConfig,
    vueConfig,
]
