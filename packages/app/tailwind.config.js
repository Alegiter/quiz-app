/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,ts}",
  ],
  theme: {
    extend: {
      height: {
        "screen-s": "100svh",
      }
    },
  },
  plugins: [],
}
