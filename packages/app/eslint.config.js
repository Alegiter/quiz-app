import { ignores, tsConfig, vueConfig } from "../../eslint.config.js"

import path from "path"
import { fileURLToPath } from "url"
import { FlatCompat } from "@eslint/eslintrc"

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const compat = new FlatCompat({
    baseDirectory: __dirname
})

const fsd = compat.extends("plugin:@conarti/feature-sliced/recommended")

const {rules: tsRules, ...tsConfigWithoutRules} = tsConfig
const {rules: vueRules, ...vueConfigWithoutRules} = vueConfig

void tsRules, vueRules

export default [
    ignores,
    tsConfigWithoutRules,
    vueConfigWithoutRules,
    ...fsd,
]