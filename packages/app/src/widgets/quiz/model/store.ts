import { defineStore } from "pinia"
import { computed, shallowRef, ref } from "vue"
import { TriviaQuestion, getTriviaQuestions } from "@/shared/api"

export const useQuizStore = defineStore("quiz", () => {
    async function loadQuiz(categoryId: number): Promise<void> {
        try {
            const values = await getTriviaQuestions({
                amount: 10,
                category: categoryId
            })
                .then(json => json.results)
            questions.value = values
        } catch (e) {
            console.error("Quiz | error", e)
            throw e
        }
    }

    const questions = shallowRef<Array<TriviaQuestion>>([])
    const correctAnswers = computed(() => {
        return questions.value.map(( item) => item.correct_answer)
    })
    const answers = ref<Array<TriviaQuestion["correct_answer"]>>([])
    const hasCurrentQuestionAnswer = computed(() => {
        return !!answers.value[currentQuestionIndex.value]
    })

    const currentQuestionIndex = ref<number>(0)
    const currentQuestion = computed(() => {
        return questions.value[currentQuestionIndex.value]
    })
    const isLastQuestion = computed(() => {
        return currentQuestionIndex.value === questions.value.length - 1
    })

    const isCompleted= ref(false)

    function nextQuiestion(): void {
        if (!hasCurrentQuestionAnswer) {
            return
        }
        if (currentQuestionIndex.value === questions.value.length - 1) {
            return
        }
        currentQuestionIndex.value++
    }

    function setAnswerToCurrentQuestion(answer: string): void {
        answers.value[currentQuestionIndex.value] = answer
    }

    function completeQuiz(): void {
        isCompleted.value = true
    }

    return {
        loadQuiz,
        questions,
        correctAnswers,
        answers,
        currentQuestion,
        hasCurrentQuestionAnswer,
        isLastQuestion,
        isCompleted,
        nextQuiestion,
        setAnswerToCurrentQuestion,
        completeQuiz,
    }
})