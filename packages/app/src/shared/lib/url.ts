export function makeQuery(obj: Record<string, number | string>): string {
    const query = Object.entries(obj)
        .reduce((query, [key, value]) => {
            return `${query}&${key}=${value}`
        }, "")
        .substring(1)
    return `?${query}`
}