import { makeUrlQuery } from "../../lib"
import { Difficulty, Categoty, Question, Type } from "./types"

function makeTriviaUrl(url: string): string {
    const normalized = url.startsWith("/") ? url : `/${url}`
    return `https://opentdb.com${normalized}`
}

type GetCategoriesResponse = {
    trivia_categories: Array<Categoty>
}

export async function getCategories(): Promise<GetCategoriesResponse> {
    const url = makeTriviaUrl("/api_category.php")
    return fetch(url)
        .then(response => response.json())
}

type GetQuestionsRequest = {
    amount: number
    category?: number
    difficulty?: Difficulty
    type?: Type
    encode?: "url3986" | "base64"
}
type GetQuestionsResponse = {
    results: Array<Question>
}

export async function getQuestions(request: GetQuestionsRequest): Promise<GetQuestionsResponse> {
    const params = makeUrlQuery(request)
    const url = makeTriviaUrl(`api.php${params}`)
    return fetch(url)
        .then(response => response.json())
}