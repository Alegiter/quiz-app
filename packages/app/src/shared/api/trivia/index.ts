export { getQuestions as getTriviaQuestions } from "./api"
export type { Question as TriviaQuestion, Categoty as TriviaCategory } from "./types"