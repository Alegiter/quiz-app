export type Question = {
    category: string
    type: Type
    difficulty: Difficulty
    question: string
    correct_answer: string
    incorrect_answers: Array<string>
}

export type Type = "multiple" | "boolean"
export type Difficulty = "easy" | "medium" | "hard"

export type Categoty = {
    id: number
    name: string
}