import { Directive } from "vue"
import DOMpurify from "dompurify"

export const vHtmlSanitized: Directive<HTMLElement> = {
    beforeMount(el, binding) {
        insertSanitizedHtml(el, binding)
    },
    beforeUpdate(el, binding) {
        insertSanitizedHtml(el, binding)
    }
}

function insertSanitizedHtml(el: HTMLElement, { value }: { value: string }) {
    const html = DOMpurify.sanitize(value)
    el.innerHTML = html
}