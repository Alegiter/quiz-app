import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import dts from "vite-plugin-dts"
import { resolve } from "path"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), dts()],
  build: {
    lib: {
      entry: resolve(__dirname, "src/index.ts"),
      name: "QuizComponentLibrary",
      fileName: "quiz-component-lib",
    },
    rollupOptions: {
      external: ["vue", "dompurify"],
      output: {
        globals: {
          vue: "Vue",
        },
      },
    },
  },
})
